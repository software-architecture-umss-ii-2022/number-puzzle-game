package backend.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;

import backend.domain.CardEntity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CardInputPortTest {

  private final int ROWS = 6;
  private final int COLUMNS = 5;

  private static final int VALUE2 = 2;
  private CardEntity cardEntity2;

  private static final int VALUE4 = 4;
  private CardEntity cardEntity4;

  private static final int VALUE8 = 8;
  private CardEntity cardEntity8;

  private static final int VALUE16 = 16;
  private CardEntity cardEntity16;

  private static final int VALUE32 = 32;
  private CardEntity cardEntity32;

  private static final int VALUE64 = 64;
  private CardEntity cardEntity64;

  private List<CardEntity> tableColumn;
  @Mock
  private ICardOutputPort cardOutputPort;

  @InjectMocks
  private CardInputPort cardInputPort;

  @BeforeEach
  public void setUp() {

    cardEntity2 = new CardEntity(VALUE2);
    cardEntity4 = new CardEntity(VALUE4);
    cardEntity8 = new CardEntity(VALUE8);
    cardEntity16 = new CardEntity(VALUE16);
    cardEntity32 = new CardEntity(VALUE32);
    cardEntity64 = new CardEntity(VALUE64);

  }

  @AfterEach
  void tearDown() {
  }

  @Test
  public void testGetCardRowById() {

  }

  @Test
  void getCardColumnById() {
  }

  @Test
  public void testSaveCardInColumn() {
    List<CardEntity> tableColumn = new ArrayList<>();
    tableColumn.add(new CardEntity(2));
    tableColumn.add(new CardEntity(4));
    tableColumn.add(new CardEntity(8));

    int columnId = 4;
    Mockito.when(cardOutputPort.fetchCardsByColumnIdOnlyWithValue(columnId))
        .thenReturn(tableColumn);
    Mockito.when(cardOutputPort.fetchSaveCardsInColumn(anyInt(), anyList()))
        .thenReturn(tableColumn);
    List<CardEntity> list = cardInputPort.saveCardInColumn(columnId, new CardEntity(2));
    assertEquals(list.size(), 1);
    assertEquals(list.get(0).getValue(), 16);
  }

  @Test
  void findValueInTable() {
  }

  @Test
  void findEmptyValueInTable() {
  }

  @Test
  void getNumberOfRows() {
  }

  @Test
  void getNumberOfColumns() {
  }
}