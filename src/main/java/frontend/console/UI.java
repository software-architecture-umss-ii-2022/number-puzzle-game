package frontend.console;
import backend.domain.EstadoJuego;
import backend.infrastructure.Juego;
import java.util.Arrays;
import java.util.Scanner;

/**
 * The name of this class should be FacadeDisplay.
 */
public class UI {

  public static final int MAX_DIGIT_NUMBER = 3;
  private final Juego juego;

  public UI(Juego juego) {
      this.juego = juego;
  }

  public void jugar() {
    printWelcomeMessage();
    Scanner sc = new Scanner(System.in);
    EstadoJuego state = EstadoJuego.enJuego;
    int next1 = juego.getSig();
    int next2 = juego.subSubSig();
    boolean validColumn = true;
    do {
        //printTable(juego.getBoardTable());
        printTable(stringToMatrix(juego.toString()));
        printNexts(next1, next2);

        System.out.print("Column: ");
        int column = sc.nextInt();
        if (juego.isValidColumn(column - 1)) {
          state = insertValue(column - 1);
          //if (state == EstadoJuego.ganado) printTable(juego.getBoardTable());
          if (state == EstadoJuego.ganado) printTable(stringToMatrix(juego.toString()));
          else if (state != EstadoJuego.sinEspacioColumna) {
            next1 = juego.getSig();
            next2 = juego.subSubSig();
          }
        } else {
          printInsertValidColumn();
        }
    } while(state != EstadoJuego.ganado);
  }
  private EstadoJuego insertValue(int value) {
    EstadoJuego state = juego.aniadirColumna(value);
    if (state == EstadoJuego.sinEspacioColumna) {
      printFullColumnMessage();
    } else if (state == EstadoJuego.ganado) {
      printWinMessage();
    } else if (state == EstadoJuego.perdido) {
      printLoseMessage();
    }
    return state;
  }

  private void printTable(String[][] matrix) {
    int rows = matrix.length;
    int columns = matrix[0].length;
    printTableTop(columns);
    for(int i=0; i<rows; i++) {
      printTableValues(matrix[i]);
    }
    printColumnIndex(columns);
  }
  private void printTableTop(int numberColumns) {
    for(int i=0; i < numberColumns; i++) {
      if (i == 0) System.out.print("┌─────┐");
      else System.out.print("─────┐");
    }
    System.out.println();
  }
  private void printTableValues(String[] row) {
    for(int i=0; i < row.length; i++) {
      if (i == 0) {
        System.out.printf("│ %s │", fixToMaxDigit(row[i]));
      } else {
        System.out.printf(" %s │", fixToMaxDigit(row[i]));
      }
    }
    System.out.println();
    printTableSeparationLine(row.length);
  }
  private void printTableSeparationLine(int numberColumns) {
    for(int i=0; i < numberColumns; i++) {
      if (i == 0) {
        System.out.print("└─────┘");
      } else {
        System.out.print("─────┘");
      }
    }
    System.out.println();
  }
  private void printColumnIndex(int numberColumns) {
    for(int i=1; i <= numberColumns; i++) {
      if (i == 1) System.out.printf("   %d   ", i);
      else System.out.printf("  %d   ", i);
    }
    System.out.println();
  }

  private void printWelcomeMessage() {
    System.out.println("┌─────┬────────────────────────────┐");
    System.out.println("│     │ WELCOME TO THE GAME!!!     │");
    System.out.println("└─────┴────────────────────────────┘\n");
  }

  private void printNexts(int next, int subsequent) {
    System.out.println(" NEXT      SUBSEQUENT");
    System.out.println("┌─────┐     ┌─────┐");
    printFixedValues(next, subsequent);
    System.out.println("└─────┘     └─────┘");
  }

  private void printFixedValues(int next, int subsequent) {
    String nextStr = fixToMaxDigit(Integer.toString(next));
    String subsequentStr = fixToMaxDigit(Integer.toString(subsequent));
    System.out.printf( "│ %s │     │ %s │\n", nextStr, subsequentStr);
  }
  private String fixToMaxDigit(String number) {
    while(number.length() < MAX_DIGIT_NUMBER)
      number = String.format(" %s", number);
    return number;
  }

  private void printFullColumnMessage() {
    System.out.println("The column is full select other column!");
  }

  private void printWinMessage() {
    System.out.println("CONGRATULATIONS, YOU WIN!!!!!");
  }

  private void printLoseMessage() {
    System.out.println("YOU LOSE!!!");
  }

  private void printInsertValidColumn() {
    System.out.println("Column no valid, try again!");
  }

  private String[][] stringToMatrix(String string) {
    String[] rows = string.split("\\.");
    String[] column = rows[0].split(";");
    String[][] matrix = new String[rows.length][column.length];
    for (int i = 0; i < rows.length; i++) {
      matrix[i] = rows[i].split(";");
    }
    return matrix;
  }
}
