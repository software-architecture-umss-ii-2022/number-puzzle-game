package backend.application;

import backend.domain.CardEntity;
import java.util.List;

public class CardInputPort implements ICardUseCase {

  private final ICardOutputPort cardOutputPort;

  public CardInputPort(ICardOutputPort cardOutputPort) {
    this.cardOutputPort = cardOutputPort;
  }

  @Override
  public List<CardEntity> getCardRowById(int rowId) {
    return cardOutputPort.fetchCardsByRowId(rowId);
  }

  @Override
  public List<CardEntity> getCardColumnById(int columnId) {
    return cardOutputPort.fetchCardsByColumnId(columnId);
  }

  @Override
  public List<CardEntity> saveCardInColumn(int columnId, CardEntity currentCard) {
    List<CardEntity> tableColumn = cardOutputPort.fetchCardsByColumnIdOnlyWithValue(columnId);
    tableColumn = joinColumnCards(currentCard, tableColumn);
    return cardOutputPort.fetchSaveCardsInColumn(columnId, tableColumn);
  }

  @Override
  public boolean findValueInTable(int value) {
    return cardOutputPort.fetchCardByValue(value) != null;
  }

  @Override
  public boolean findEmptyValueInTable() {
    return cardOutputPort.fetchFindEmptyCard();
  }

  @Override
  public int getNumberOfRows() {
    return cardOutputPort.fetchNumberOfRows();
  }

  @Override
  public int getNumberOfColumns() {
    return cardOutputPort.fetchNumberOfColumns();
  }

  /*
  private List<CardEntity> fillColumnWithNullValue(List<CardEntity> column) {
    int rows = cardOutputPort.fetchNumberOfRows();
    while(column.size() < rows) column.add(0, null);
    return column;
  }
   */

  private List<CardEntity> joinColumnCards(CardEntity current, List<CardEntity> list) {
    if(list.isEmpty()) {
      list.add(current);
    } else {
      CardEntity top = list.remove(0);
      if(current.equals(top)) {
        current.setValue(current.getValue() + top.getValue());
        joinColumnCards(current, list);
      } else {
        list.add(0, top);
        list.add(0, current);
      }
    }
    return list;
  }

  /*
  private List<CardEntity> joinRowsCards(CardEntity current, List<CardEntity> list) {

  }
  */
}
