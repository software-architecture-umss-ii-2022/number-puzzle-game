package backend.application;

import backend.domain.CardEntity;
import java.util.List;

public interface ICardUseCase {

  List<CardEntity> getCardRowById(int rowId);
  List<CardEntity> getCardColumnById(int columnId);
  List<CardEntity> saveCardInColumn(int columnId, CardEntity card);
  boolean findValueInTable(int value);
  boolean findEmptyValueInTable();
  int getNumberOfRows();
  int getNumberOfColumns();
}
