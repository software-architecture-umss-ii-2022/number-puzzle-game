package backend.application;

import backend.domain.CardEntity;
import java.util.List;

public interface ICardOutputPort {

  List<CardEntity> fetchCardsByColumnId(int columnId);
  List<CardEntity> fetchCardsByColumnIdOnlyWithValue(int columndId);

  List<CardEntity> fetchCardsByRowId(int rowId);
  CardEntity fetchCardByValue(int value);
  boolean fetchFindEmptyCard();

  List<CardEntity> fetchSaveCardsInColumn(int columnId, List<CardEntity> list);

  int fetchNumberOfRows();

  int fetchNumberOfColumns();
}
