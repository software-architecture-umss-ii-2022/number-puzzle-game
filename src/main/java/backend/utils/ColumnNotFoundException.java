package backend.utils;

public class ColumnNotFoundException extends RuntimeException {

  public ColumnNotFoundException(int columnId) {
    super(String.format("Column Not Found with the id: %s", Integer.toString(columnId)));
  }
}
