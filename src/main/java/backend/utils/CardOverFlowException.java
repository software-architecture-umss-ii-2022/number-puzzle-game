package backend.utils;

public class CardOverFlowException extends RuntimeException {

  public CardOverFlowException(int columndID) {
    super(String.format("Card inserted in the column %s", Integer.toString(columndID)));
  }
}
