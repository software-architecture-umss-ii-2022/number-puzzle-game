package backend.domain;

/**
 * This Module isn't an Entity it's an Value Object
 **/
public enum EstadoJuego {
  ganado, perdido, sinEspacioColumna, enJuego, jugadaNoValida;
}
