package backend.domain.service;

public class RandomValueGenerator {

  public static int generate() {
    int randomNumber = (int) (Math.random() * 5) + 1;
    return (int) Math.pow(2, randomNumber);
  }
}
