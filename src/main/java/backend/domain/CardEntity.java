package backend.domain;

import backend.domain.service.RandomValueGenerator;

public class CardEntity {

  private int value;
  public CardEntity(int value) {
    this.value = value;
  }

  public CardEntity() {
    this.value = RandomValueGenerator.generate();
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public boolean equals(Object object) {
    CardEntity card = (CardEntity) object;
    return value == card.value;
  }
}
