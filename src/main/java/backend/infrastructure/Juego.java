package backend.infrastructure;

import backend.Database;
import backend.application.CardInputPort;
import backend.application.ICardOutputPort;
import backend.application.ICardUseCase;
import backend.domain.CardEntity;
import backend.domain.EstadoJuego;
import backend.domain.service.RandomValueGenerator;
import backend.utils.CardOverFlowException;
import backend.utils.ColumnNotFoundException;

/**
 * This class can be called: GameAdapter
 * This is a Primary adapter this means that this class has access to Input Port
 */
public class Juego {

  public final static int TARGET_WIN_VALUE = 256;
  public final static int ROWS = 6;
  public final static int COLUMNS = 5;

  private int next;
  private int subsequent;

  private ICardUseCase cardUseCase;
  private static final String EMPTY_VALUE = " ";
  public Juego() {

    Database database = new Database(ROWS, COLUMNS);
    ICardOutputPort secundaryAdapter = new SecundaryAdapter(database);
    setCardUseCase(new CardInputPort(secundaryAdapter));

    subsequent = RandomValueGenerator.generate();
    next = RandomValueGenerator.generate();
  }

  //This method needs to be as parameter of the constructor and not as a setter
  //If we're using a set method this need to be injected but the rules said that Main is static.
  public void setCardUseCase(ICardUseCase cardUseCase) {
    this.cardUseCase = cardUseCase;
  }

  public EstadoJuego getEstado() {
    EstadoJuego state;
    boolean win = cardUseCase.findValueInTable(TARGET_WIN_VALUE);
    if (win) state = EstadoJuego.ganado;
    else if(cardUseCase.findEmptyValueInTable()) state = EstadoJuego.enJuego;
    else state = EstadoJuego.perdido;
    return state;
  }

  public int getSig() {
    next = subsequent;
    return next;
  }
  public int subSubSig() {
    subsequent = RandomValueGenerator.generate();
    return subsequent;
  }

  /*
  public int[] getNexts() {
    next = subsequent;
    subsequent = RandomValueGenerator.generate();
    return new int[]{next, subsequent};
  }
  */

  public String toString() {
    int rows = cardUseCase.getNumberOfRows();
    //int columns = cardUseCase.getNumberOfColumns();
    String stringMatrix = "";
    for(int i=0; i<rows; i++) {
      int j = 0;
      for (CardEntity card : cardUseCase.getCardRowById(i)) {
        if(card == null){
          stringMatrix = stringMatrix.concat(EMPTY_VALUE);
        } else {
          String value = Integer.toString(card.getValue());
          stringMatrix = stringMatrix.concat(value);
        }
        stringMatrix = stringMatrix.concat(";");
        j++;
      }
      stringMatrix = stringMatrix.concat(".");
    }
    return stringMatrix;
  }

  public EstadoJuego aniadirColumna(int columna) {

    try {
      try {
        cardUseCase.saveCardInColumn(columna, new CardEntity(next));
        return getEstado();
      } catch (CardOverFlowException cardOverFlowException) {
        return EstadoJuego.sinEspacioColumna;
      }
    } catch (ColumnNotFoundException e) {
      throw new NullPointerException("Column Not Found");
    }
  }

  public String[][] getBoardTable() {
    int rows = cardUseCase.getNumberOfRows();
    int columns = cardUseCase.getNumberOfColumns();
    String[][] stringMatrix = new String[rows][columns];
    for (int i = 0; i < rows; i++) {
      int j = 0;
      for (CardEntity card : cardUseCase.getCardRowById(i)) {
        if (card == null)
          stringMatrix[i][j] = EMPTY_VALUE;
        else
          stringMatrix[i][j] = Integer.toString(card.getValue());
        j++;
      }
    }
    return stringMatrix;
  }

  public boolean isValidColumn(int column) {
    int columns = cardUseCase.getNumberOfColumns();
    return column >= 0 && column < columns;
  }
}
