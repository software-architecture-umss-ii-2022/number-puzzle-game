package backend.infrastructure;

import backend.Database;
import backend.application.ICardOutputPort;
import backend.domain.CardEntity;
import backend.utils.CardOverFlowException;
import backend.utils.ColumnNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SecundaryAdapter implements ICardOutputPort {

  private final Database database;

  public SecundaryAdapter(Database database) {
    this.database = database;
  }

  @Override
  public List<CardEntity> fetchCardsByColumnId(int columnId) {
    CardEntity[][] matrix = database.getCardMatrix();
    List<CardEntity> list = new ArrayList<>();
    for (CardEntity[] cardEntities : matrix)
      list.add(cardEntities[columnId]);
    return list;
  }

  @Override
  public List<CardEntity> fetchCardsByColumnIdOnlyWithValue(int columnId) {
    try {
      List<CardEntity> list = fetchCardsByColumnId(columnId);
      List<CardEntity> listWithValues = new ArrayList<>();
      list.forEach(card -> {
        if (card != null)
          listWithValues.add(card);
      });
      return listWithValues;
    } catch (IndexOutOfBoundsException e) {
      throw new ColumnNotFoundException(columnId);
    }
  }

  @Override
  public List<CardEntity> fetchCardsByRowId(int rowId) {
    CardEntity[][] matrix = database.getCardMatrix();
    int columns = matrix[0].length;
    List<CardEntity> list = new ArrayList<>();
    for(int j=0; j < columns; j++)
      list.add(matrix[rowId][j]);
    return list;
  }

  @Override
  public CardEntity fetchCardByValue(int value) {
    CardEntity[][] matrix = database.getCardMatrix();
    for (CardEntity[] cardEntities : matrix)
      for(CardEntity card : cardEntities)
        if(card != null && card.getValue() == value) return card;
    return null;
  }

  @Override
  public boolean fetchFindEmptyCard() {
    CardEntity[][] matrix = database.getCardMatrix();
    for (CardEntity[] cardEntities : matrix)
      for(CardEntity card : cardEntities)
        if(card == null) return true;
    return false;
  }

  @Override
  public List<CardEntity> fetchSaveCardsInColumn(int columnId, List<CardEntity> list) {
    Collections.reverse(list);
    CardEntity[][] matrix = database.getCardMatrix();
    setEmptyColumn(columnId, matrix);
    try {
      int i = matrix.length - 1;
      for (CardEntity card : list) {
        matrix[i][columnId] = card;
        i--;
      }
      return list;
    } catch (IndexOutOfBoundsException exception) {
      throw new CardOverFlowException(columnId);
    }
  }

  @Override
  public int fetchNumberOfRows() {
    return database.getLimitRows();
  }

  @Override
  public int fetchNumberOfColumns() {
    return database.getLimitColumns();
  }

  private void setEmptyColumn(int columnId, CardEntity[][] matrix) {
    for (CardEntity[] row : matrix) {
      row[columnId] = null;
    }
  }
}
