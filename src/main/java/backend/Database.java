package backend;

import backend.domain.CardEntity;

public class Database {

  private final CardEntity[][] cardMatrix;
  private final int columns;
  private final int rows;

  public Database(int rows, int columns) {
    this.columns = columns;
    this.rows = rows;
    cardMatrix = new CardEntity[rows][columns];
    initializateMatrix(cardMatrix);
  }

  //Bad method.
  /*
  public boolean insertCard(CardEntity card, int column, int row) {
    cardMatrix[column][row] = card;
    return true;
  }
  */

  public CardEntity[][] getCardMatrix() {
    return cardMatrix;
  }

  public int getLimitColumns() {
    return columns;
  }

  public int getLimitRows() {
    return rows;
  }

  private void initializateMatrix(CardEntity[][] matrix) {
    int rows = matrix.length;
    int columns = matrix[0].length;
    for(int i=0; i < rows; i++)
      for(int j=0; j < columns; j++)
        matrix[i][j] = null;
  }
}
