import backend.infrastructure.Juego;
import frontend.console.UI;

public class Main {
  public static void main(String[] args) {
    Juego juego = new Juego();
    UI ui = new UI(juego);
    ui.jugar();
  }
}
